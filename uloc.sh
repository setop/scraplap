#!/bin/sh

find . \( -name "*.py" -or -name "*.md" -or -name "*.sh" -or -name "*.sql" -or -name "Dockerfile" \) -and -not \( -path "./.venv/*" -or -path "./.vscode/*" \) -print0 | \
xargs -0 cat | countuniq0  # sort -u | wc -l