# How

The tool leverage on two great tools:
* [scrapy](https://scrapy.org/) as a web crawler (it can do much more)
* [readability](https://pypi.org/project/readability-lxml/) as a way to extract relevant content from now mostly bloated web pages.

The tool basically performs three tasks : 
1. going through the original news feed and downloading linked content.
1. build a destination feed with the same structure as the original feed but with cleaned content instead of link.
1. upload the destination feed to a hosting web server via ftp.

## early version

Scrapy is able to perform these three task in one run, thanks to its modular architecture. In the early version of this tool, the two first tasks were performed in the crawler.

```
      original feed                       destination feed
7: id, title, date, link  \    / 3: id, title, link, readable content
6: id, title, date, link  |    | 1: id, title, link, readable content
5: id, title, date, link  |    | 6: id, title, link, readable content
4: id, title, date, link  | => | 2: id, title, link, readable content
3: id, title, date, link  |    | 7: id, title, link, readable content
2: id, title, date, link  |    | 4: id, title, link, readable content
1: id, title, date, link  /    \ 5: id, title, link, readable content
```

### shuffling issue

The shuffling is a consequence of the threading capability of scrapy.

This is even worse as the order in the original feed gets modified by the popularity of the content (this is how lobsters and fellows work).

```
      original feed                       destination feed
8: id, title, date, link  \    / 1: id, title, link, readable content
6: id, title, date, link  |    | 9: id, title, link, readable content
7: id, title, date, link  |    | 6: id, title, link, readable content
9: id, title, date, link  | => | 3: id, title, link, readable content
5: id, title, date, link  |    | 5: id, title, link, readable content
1: id, title, date, link  |    | 7: id, title, link, readable content
3: id, title, date, link  /    \ 8: id, title, link, readable content
```

### performance issue

Scrapy provide HTTP caching feature. This helps not downloading content again and again.

However the most CPU consuming workload is done by readability. Most of the time, between two runs, only few new contents appear in the original feed, but the tool runs readability over all contents each time. This calls for a caching mechanism.


## redesign

In next release we want to bring new features and improvements:
* keep content in publication order,
* keep former contents in the destination feed
* reduce run time
* publishing destination feed to multiple locations, in multiple forms

In order to achieve that, 
1. the tool is split into three sub-tools : crawler, feed builder, uploader.
1. the data are persisted in a sqlite database.
1. database is used to know if a content is known or new

Database contents one table which looks like :

```
1: id, title, date, link, content-type, readable content
2: id, title, date, link, content-type, readable content
3: id, title, date, link, content-type, readable content
4: id, title, date, link, content-type, readable content
5: id, title, date, link, content-type, readable content
6: id, title, date, link, content-type, readable content
7: id, title, date, link, content-type, readable content
8: id, title, date, link, content-type, readable content
9: id, title, date, link, content-type, readable content
```

It provides more flexibility:
* only new contents are fetched and processed
* readability workload is reduced
* destination feed can be build decoupled from the original feed, using a simple query : `select * from entry order by date DESC, id, limit N`

The tool, the three sub-tools, are still packed in a unique docker image.