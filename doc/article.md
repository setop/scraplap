Bonjour nal,

Je prends la plume pour présenter **scraplap**.

Cet outil n'avait pas de nom à sa création. Mais pour se présenter, un nom est bien utile.
J'ai donc "inventé" le terme _LAP_, pour _link aggregation platform_ ; une plateforme qui agrège des liens vers du contenu sur un sujet plus ou moins large et qui permet bien souvent de les noter et les commenter. On peut penser à Reddit, à Lobsters, à Journal du hacker.
_Scrap_ est quant à lui le diminutif de _scrapy_, la [bibliothèque python](https://scrapy.org/) que j'utilise pour faire le [scraping](https://fr.wikipedia.org/wiki/Web_scraping).

**Scraplap** s'inscrit dans la mouvance _[weboob](http://weboob.org/)_, qui consiste à permettre des usages web hors navigateur (gérer sa banque, faire des rencontres, etc.). Pour ma part je suis intéressé par l'usage _contentoob_ : consulter du contenu produit pour le web hors du navigateur. Dans ce domaine, les formats rss/atom et epub sont très utiles pour proposer du contenu sans passer par le navigateur. Ils permettent de ne pas avoir à développer d'application cliente spécifique ; une tâche trop ardue à mon goût. Il y a de très bon [lecteurs RSS](https://en.wikipedia.org/wiki/Comparison_of_feed_aggregators), sur toutes les plateformes.

Une LAP comme [Lobsters](https://lobste.rs/) propose déjà un [fil d'actualités](https://lobste.rs/rss) en RSS. Cependant, ils ne s'autorisent pas à y inclure du contenu qui n'est pas produit par la plateforme. On y retrouve donc que les liens vers les articles, pas les articles eux-mêmes. Or c'est typiquement là que je souhaite du contentoob. Je ne souhaite pas sortir du lecteur de fils et devoir être en ligne pour lire l'article. Scraplap fait ce travail pour moi. Il parcourt le fil d'actualité de la LAP, va chercher les articles en ligne et les remets dans un nouveau flux.

![fil d'actu](fil.png)

Pour que les articles soient lisibles dans le lecteur de fils d'actu, l'outil applique le même algorithme que le [mode lecteur de Firefox](https://github.com/mozilla/readability), grâce à une [implémentation python](https://pypi.org/project/readability-lxml/) basée sur lxml. Ce mécanisme mériterai un article à part entière.

![article](harticle.png)

**Scraplap** a connu deux releases. La première, très intimiste, était stateless, elle lisait le fil source et produisait le fil résultat en même temps. Cela posait plein de problèmes qui sont expliqués dans [ce document de design](https://framagit.org/setop/scraplap/blob/master/DESIGN.md) ; désolé, c'est en anglais. La release actuelle est statefull, elle récupère les nouveaux articles, les stocke en DB, puis construit le fil résultat.

Le code est [disponible](https://framagit.org/setop/scraplap) sur Framagit, l'instance gitlab de Framasoft. J'en ai profité pour mettre en place un don récurent car je veux soutenir leur initiative de dégafamisation. Les fils d’actu sont disponibles sur mon hosting, pour [lobsters](http://www.zoocoop.com/contentoob/lobsters.atom) et pour [journalduhacker](http://www.zoocoop.com/contentoob/journalduhacker.atom).

Si ce type d'outils vous intéresse, vous pouvez alimenter la backlog, faire des PR, commenter.
