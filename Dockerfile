FROM python:3.11-slim

RUN apt-get update && \
  apt-get install --assume-yes --no-install-recommends cron sqlite3 curl ftp && \
  rm -rf \
    /var/cache/apt/archives/*.deb /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/log/* /usr/share/doc/* /var/lib/dpkg/info/*

COPY requirements.txt .

RUN pip --no-cache-dir install -r requirements.txt

WORKDIR /scraplap

COPY scripts scripts
COPY lobsters lobsters
COPY scrapy.cfg .

ENTRYPOINT /scraplap/scripts/entrypoint.sh
