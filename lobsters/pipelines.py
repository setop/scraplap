import logging
from readability.readability import Document
from time import strptime

logger = logging.getLogger(__name__)

RFC_2822 = '%a, %d %b %Y %H:%M:%S %z'


class LobstersPipeline(object):
    def process_item(self, item, spider):
        item["pubdate"] = strptime(item["pubdate"], RFC_2822)
        # item["contenttype"] can be null if no 'Content-Type' header was present
        item["contenttype"] = item["contenttype"].decode("ASCII").split(";")[0] if item["contenttype"] is not None else "text/html" 
        if item["contenttype"] == "text/html":
            logger.info("run readability on %s" % item["id"])
            doc = Document(item["content"], url=item["link"])
            item["content"] = doc.summary(html_partial=True)
        elif item["contenttype"] == "text/plain":
            pass
        else:
            item["content"] = None
        return item


def test():
	none_contenttype_test = { # issue #7
		'id': 'fbaek3',
		'title': 'The ACUTE-TERMINAL-CONTROL Common Lisp Library',
		'updated': 'Sat, 1 Dec 2018 22:05:35 -0100',
		'link': 'http://verisimilitudes.net/2018-04-04',
		'contenttype': b"void/void",
		'content': b'',
		'expected_content': None,
	}
	basic_test = {
		'id': "jv9g78",
		'title': "Installer un serveur Firefox Sync",
		'updated': "Wed, 24 Oct 2018 16:36:03 +0200",
		'link': "https://blog.cloudflare.com/every-7-8us-your-computers-memory-has-a-hiccup/",
		'contenttype': b"void/void",
		'content': b'',
		'expected_content': None,
	}
	html_test = {
		'id':'di5vin',
			'title' : 'Shared personal note taking?',
		'updated':'Mon, 27 May 2019 05:54:10 +0200',
		'link':'https://lobste.rs/s/di5vin/shared_personal_note_taking',
		'contenttype': b"text/html; charset=UTF-8",
		'content': open("tests/in.html").read(),
		'expected_content':  open("tests/out.html").read(),
	}
	for t in [basic_test, none_contenttype_test, html_test]:
		item = LobstersPipeline().process_item(t, None)
		assert item["content"] == item["expected_content"]
		#print(__import__("json").dumps(item))


if __name__ == "__main__":
    test()
