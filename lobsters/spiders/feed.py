# -*- coding: utf-8 -*-
import logging

from urllib.parse import urlparse

from scrapy.spiders import XMLFeedSpider
from scrapy import Request
from scrapy.extensions.httpcache import DummyPolicy

import sqlite3

import lobsters.settings as settings

__all__ = ['AllButStartURLPolicy']

logger = logging.getLogger(__name__)


class AllButStartURLPolicy(DummyPolicy):

    __slot__ = ['feedurl']

    def __init__(self, settings):
        super(AllButStartURLPolicy, self).__init__(settings)
        self.feedurl = urlparse(globals()["feedurl"])

    def should_cache_request(self, request):
        (_, netloc, path, _, _, _) = urlparse(request.url)
        if netloc == self.feedurl.netloc and path == self.feedurl.path:
            logger.info('will not cache %s' % (request.url))
            return False
        else:
            return super().should_cache_request(request)


class FeedSpider(XMLFeedSpider):
    name = 'feed'

    def __init__(self, feedurl=None, feedid=None, *args, **kwargs):
        super(FeedSpider, self).__init__(*args, **kwargs)
        self.start_urls = ['%s' % feedurl]
        self.feedid = feedid
        globals()["feedurl"] = feedurl
        self.open_db()

    def open_db(self):
        settings.db = sqlite3.connect(settings.DATA_DIR+"/"+self.feedid+".db")

    def exists(self, itemid):
        c = settings.db.cursor()
        c.execute("select count(1) from entry where id = ?", (itemid,))
        r = c.fetchone()
        c.close()
        return r[0] > 0

    @staticmethod
    def find_an_id(node):
        guid = node.xpath('guid/text()').extract_first()
        def find_an_id_in(where): #
            logger.debug("find in "+where)
            def f(node):
                guid = node.xpath(where+'/text()').extract_first()
                return guid.split("/")[-1]
            return f
        FeedSpider.find_an_id = find_an_id_in(["guid","comments"][guid is None])
        return FeedSpider.find_an_id(node)

    def parse_node(self, response, node):
        node.remove_namespaces()
        itemid = FeedSpider.find_an_id(node)
        if not self.exists(itemid):
            i = dict()
            i['id'] = itemid  # or guid ?
            i['title'] = node.xpath('title/text()').extract_first()
            i['pubdate'] = node.xpath('pubDate/text()').extract_first()
            target = node.xpath('link/text()').extract_first()
            i['link'] = target
            i['tags'] = node.xpath('category/text()').getall()  # a list
            stg = target.split('/')
            fqdn = stg[2]
            if fqdn == "github.com":  # https://github.com/AlexCoursi/jarvis-deezer, try to get README
                logger.debug("special process for github.com")
                target = f'https://raw.githubusercontent.com/{stg[3]}/{stg[4]}/main/README.md'
            elif fqdn == "twitter.com":  # https://twitter.com/AlexArchambault/status/1640583192913559553
                logger.debug("special process for twitter.com")
                target = f'https://nitter.tiekoetter.com/{stg[3]}/status/{stg[5]}'
            elif fqdn == "medium.com":  # https://medium.com/rebirth-delivery/elixir-newb-how-to-get-columns-not-nil-in-ecto-5e7186888444
                logger.debug("special process for medium.com")
                target = f'https://scribe.rip/{stg[3]}/{stg[4]}'
            r = Request(target, self.parse_target)
            r.meta["i"] = i
            return r
        return None

    def parse_target(self, response):
        i = response.request.meta["i"]
        contenttype = response.headers.get('Content-Type')
        i["contenttype"] = contenttype
        i["content"] = response.body
        return i

    def close(self, reason):
        settings.db.close()
