# -*- coding: utf-8 -*-

BOT_NAME = 'FEED'

SPIDER_MODULES = ['lobsters.spiders']
NEWSPIDER_MODULE = 'lobsters.spiders'


# some privacy
UER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0'
ROBOTSTXT_OBEY = False
REFERER_ENABLED = False
COOKIES_ENABLED = False


# Disable Telnet Console (enabled by default)
TELNETCONSOLE_ENABLED = False


DOWNLOAD_TIMEOUT=10

# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
    'lobsters.pipelines.LobstersPipeline': 300,
}


DATA_DIR = __import__("os").environ["DATA_DIR"]

FEED_EXPORTERS = {
    'db': 'lobsters.exporters.DBFeedExporter',
}

db = None


# Enable and configure HTTP caching (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
HTTPCACHE_ENABLED = False  # use cache in dev when debug, in prod it is not needed
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
HTTPCACHE_POLICY = 'lobsters.spiders.feed.AllButStartURLPolicy'
HTTPCACHE_IGNORE_HTTP_CODES = [500, 503]
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'
