from scrapy.exporters import BaseItemExporter
from time import mktime
import sqlite3
import lobsters.settings as settings


def build_insert(table:str, item:dict):
	columns = ",".join(item.keys())
	placeholders = ",".join("?"*len(item))
	values = tuple(item[k] for k in item.keys())
	return "insert into %s (%s) values (%s)" % (table,columns,placeholders), values


class DBFeedExporter(BaseItemExporter):
	def __init__(self, file, **kwargs):
		self._configure(kwargs, dont_fail=True)
		self.file = file

	def export_item(self, item):
		conn = settings.db
		item["pubdate"] = int(mktime(item["pubdate"]))
		item["tags"] = ",".join(item["tags"])
		conn.execute(*build_insert("entry",item))
		conn.commit()


def test():
	conn = sqlite3.connect("test_exporter.db")
	settings.db = conn
	from lobsters.pipelines import RFC_2822
	from time import strptime
	basic_test = {
		'id': "jv9g78",
		'title': "Installer un serveur Firefox Sync",
		'pubdate': strptime("Sat, 24 Oct 2020 16:36:03 +0200", RFC_2822),
		'link': "https://blog.cloudflare.com/every-7-8us-your-computers-memory-has-a-hiccup/",
		'contenttype': b"void/void",
		'content': b'',
		'tags': ["foo","bar"],
	}
	DBFeedExporter(None).export_item(basic_test)
	conn.commit()
	conn.close()


if __name__ == '__main__':
	test()
