import sys
import sqlite3
from tqdm import tqdm
from contextlib import contextmanager
import gzip


table="entry"
columns=["id", "title", "pubdate", "link", "contenttype", "content", "tag"]


def iterate_entries(db_path):
	conn = sqlite3.connect(db_path)
	total = conn.execute("select count(1) from entry").fetchone()[0]
	cur = conn.execute("select "+",".join(columns)+" from entry")
	for r in tqdm(cur, total=total): #22755):
		yield r
	conn.close()


@contextmanager
def foo(db_path):
	conn = sqlite3.connect(db_path)
	part1 = ",".join(columns)
	part2 = ",".join("?"*len(columns))
	ps = "insert into %s (%s) values (%s)" % (table,part1,part2)
	n = 1
	def push_entry(t):
		nonlocal n
		conn.execute(ps,t)
		if (n%100) == 0:
			conn.commit()
		n+=1
		return
	try:
		yield push_entry
	finally:
		conn.commit()
		conn.close()


def compress_content(t):
	l = list(t)
	if l[-2] is not None:
		try:
			l[-2] = gzip.compress(l[-2].encode())
		except:
			(etype, value, _) = sys.exc_info()
			print(etype, value)
			import traceback
			traceback.print_exc(file=sys.stderr)
	return tuple(l)


def main(db_src, db_dest):
	with  foo(db_dest) as pusher:
		for entry in iterate_entries(db_src):
			entry = compress_content(entry)
			pusher(entry)


if __name__ == '__main__':
	main(sys.argv[1], sys.argv[2])
