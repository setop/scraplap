# -*- coding: utf-8 -*-

from scrapy.spiders import Spider
from scrapy import Request

from time import strptime, strftime


RFC_2822 = '%a, %d %b %Y %H:%M:%S %z'
date_format = '%Y-%m-%d %H:%M:%S %z'


class Paginator(Spider):
    name = 'paginator'
    custom_settings = {
        "USER_AGENT" : 'Mozilla/5.0 (X11; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0',
        "ROBOTSTXT_OBEY" : False,
        "REFERER_ENABLED" : False,
        "COOKIES_ENABLED" : False,
        "TELNETCONSOLE_ENABLED" : False,
        "HTTPCACHE_ENABLED" : True,
    }

    base_url = "https://lobste.rs/"
    page_url = base_url+"page/%d"
    start_urls = [ base_url ]

    def parse(self, response):
        for p in range(1,11+1):
            yield Request(self.page_url % p, self.parse_page)

    def parse_page(self, response):
        liS = response.xpath('//li/attribute::data-shortid').getall()
        hrefS =  response.xpath('//a[@class="u-url"]/attribute::href').getall()
        titleS = response.xpath('//a[@class="u-url"]/text()').getall()
        dateS = response.xpath('//div[@class="byline"]/span/attribute::title').getall()
        print(list(map(len, [liS, hrefS, titleS, dateS])))
        for (li, href, date, title) in zip(liS, hrefS, dateS, titleS):
            yield { "id": li, "link": href, "updated": strftime(RFC_2822, strptime(date, date_format)), "title": title}
