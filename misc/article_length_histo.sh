#!/bin/sh
sqlite3 out/lobsters.db 'select length(content) from entry' |\
awk '{b=2; a=sprintf("%.0f", log($1)/log(b)); print b^a}' | sort -n | uniq -c | awk '{print $2","$1}' | jp -input csv -type bar # |\
#less
