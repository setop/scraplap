from lxml import etree, html


def print_tree(e:etree, depth=0):
	print(" "*depth, e.tag, "=>", e.text[:60] if e.text is not None else "")
	for c in e:
		print_tree(c, depth+1)

if __name__ == "__main__":
	import sys
	print_tree(html.fromstring(sys.stdin.read()))# parse(sys.stdin)# 
