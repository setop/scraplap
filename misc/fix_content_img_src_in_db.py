import sys
import sqlite3
import lxml.html as lh
import lxml
from urllib.parse import urlparse, urlunparse


def to_absolute_path(b, doc, elem, attr):
    (bscheme, bnetloc, bpath) = b
    for (ele_idx, ele) in enumerate(doc.xpath("//"+elem)):
        src = ele.get(attr)
        if src is None:
            print("WARN : elem %s n°%d as no %s attribute " % (elem, ele_idx, attr), file=sys.stderr)
        else:
            (scheme, netloc, path, _, _, _) = urlparse(src)
            print(elem, src, scheme, netloc, path, file=sys.stderr)
            if len(netloc) == 0:
                if len(path) > 0 and path[0] != "/":
                    path = "/".join(bpath.split("/")[:-1] + path.split("/"))
                src = urlunparse((bscheme, bnetloc, path, "", "", ""))
                ele.set(attr, src)
    
    

def main():
    conn1 = sqlite3.connect(sys.argv[1])
    conn2 = sqlite3.connect(sys.argv[2])
    cur = conn1.execute("select * from entry")
    for (entry_id, t, p, l, ct, c) in cur:
        if ct == "text/html":
            print(entry_id, l, file=sys.stderr)
            (bscheme, bnetloc, bpath, _, _, _) = urlparse(l)
            try:
                c = lh.fragment_fromstring(c)
            except :
                c = lh.document_fromstring(c)
            to_absolute_path((bscheme, bnetloc, bpath), c, "a", "href")
            to_absolute_path((bscheme, bnetloc, bpath), c, "img", "src")
            c = lh.tostring(c).decode("ascii")
        conn2.execute("insert into entry values (%s)" % ",".join("?"*6), (entry_id, t, p, l, ct, c))
    conn2.commit()
    conn2.close()
    conn1.close()


if __name__ == '__main__':
    main()
