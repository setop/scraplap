
import sys
import sqlite3


def main():
    conn = sqlite3.connect(sys.argv[1])
    cur = conn.execute("select * from entry")
    for (i, _, _, _, ct, c) in cur:
        if ct == "text/html":
            with open("out/lobsters/%s.%s" % (i, ct.split("/")[1]), "w") as f:
                f.write(c)
    conn.close()


if __name__ == '__main__':
    main()
