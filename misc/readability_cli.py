import sys
from readability.readability import Document


if __name__ == "__main__":
    doc = Document(sys.stdin.read())
    sys.stdout.write(doc.summary(html_partial=True))
