#!/bin/sh -eu

CMDD=$(dirname "$(realpath $0)")

FEED_ID="$1"
FEED_URL="$2"

. $CMDD/env/env
[ -s $CMDD/env/$1.env ] && . $CMDD/env/${FEED_ID}.env

read -r -p "run for $FEED_ID ? (CTRL=c to cancel, any key to continue)"

# 0: init db if needed
sqlite3 $DATA_DIR/${FEED_ID}.db < $SCRIPTS/schema.sql

# 1: scrap data
scrapy crawl -L $LOG_LEVEL -t db -o /dev/null feed -a feedurl="$FEED_URL" -a feedid="$FEED_ID"

# 2:  build feed
export PYTHONPATH="$SCRIPTS/.."
$SCRIPTS/build_atom.py $FEED_ID "${DATA_DIR}/${FEED_ID}.db" $FEED_NB_ENTRIES | gzip > "out/${FEED_ID}.atom.gz"

# 3: upload
read -r -p "upload ? (CTRL=c to cancel, any key to continue)" && $SCRIPTS/ftpupload.sh "out/${FEED_ID}.atom.gz" "${FTP_DIR}"
