from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support import expected_conditions as expected
from selenium.webdriver.support.wait import WebDriverWait

def ff_screenshot(url:str, output):
    options = Options()
    options.add_argument('-headless')
    driver = Firefox(executable_path='/home/mrloyal/Sync/scraplap/readability_issues/geckodriver-v0.24.0', options=options)
    #wait = WebDriverWait(driver, timeout=1)
    driver.get(url)
    #wait.until(expected.visibility_of_element_located((By.NAME, 'q'))).send_keys('headless firefox' + Keys.ENTER)
    #wait.until(expected.visibility_of_element_located((By.CSS_SELECTOR, '#ires a'))).click()
    #print(driver. get_cookies())
    output.write(driver.page_source)
    driver.quit()	

if __name__ == "__main__":
	import sys
	ff_screenshot(sys.argv[1], sys.stdout)
