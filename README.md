
# what

Link aggregation platforms like [Lobsters](https://lobste.rs/) are very convenient to get valuable content about one particular center of interest - like computer science in the case of Lobsters.
Even better, they propose news feed which can be added to your preferred feed reader in order to get notifications of new contents and read them offline.

The only bothering thing to me is that platforms do not allow themselves to inline linked articles in their feed so I have to go online to read them.
This kind of ruins the offline reading experience.

This tool reads the platform feed, loads all linked articles and rebuild the feed which articles inlined. So you can enjoy reading articles while commuting.


# build, deploy and run

This tool supports two way of running: directly on the host or in a container using docker.

## configuration

Both ways are using a ".env" file to set configuration.

The following variables have to be set :

* scraper variables

| variable | usage | example |
| -- | -- |  -- |
|DATA_DIR|where data will be produced|/var/scrapy|
| LOG_LEVEL | log verbosity for python logger | INFO |
| FEED_ID | identify feed, used everywhere | lobsters or journalduhacker |
| FEED_URL | url of the original feed | https://lobste.rs/rss or https://www.journalduhacker.net/rss |

* feed builder variables

| variable | usage | example |
| -- | -- |  -- |
|FEED_NB_ENTRIES|number of entries in the destination feed|40|


* ftp variables

| variable | usage | example |
| -- | -- |  -- |
|FTP_SERVER|where to put feed|ftp.provider.net|
|FTP_USER|ftp user|webhostinguser|
|FTP_PASS|password|crypticphrase|
|FTP_DIR|directory, must exists|www/feeds|

## locally 

### dependencies

* python3
* python scrapy
* python readability-lxml
* ftp client (like netkit-ftp on debian)

You also need an access to a web server via ftp.

### get application

git clone this repo then cd inside.

### run

Create ".env" file with variables seen in configuration part as `export KEY="VALUE"` lines.

To scrap, build feed and upload it, run : `./scripts/runscrap.sh <feed_id> <feed_url>`

This could be put in a crontab.

## using docker

Here is a basic way to deploy this tool on docker. You infrastructure may be much more elaborated than what is proposed here.

### build

git clone this repo then cd inside.

`rm -rf .git* doc tests *.md`

`docker build -t scraplap .`


### deploy

move the image to target environment

`docker save scraplap:latest | pv | ssh <prod_host> docker load`

### run

`useradd -b /var scrapy`

Create ".env" file with variables seen in configuration part as `KEY=VALUE` lines.

To scrap, build feed and upload it, run :

```
docker run --rm -u $(id -u scrapy):$(id -g scrapy) \
   --volume /var/scrapy:/var/scrapy \
   --volume /var/scrapy:/scraplap/.scrapy \
   scraplap
```

Feed file and logs are produced on the host in `/var/scrapy`.


# TODO

* content :
  * handle formats other than text and html, like pdf

* core enhancement
  * handle other patterns than feed -> article
