#!/bin/sh -eu

CMDD=$(dirname "$(realpath $0)")

$CMDD/runscrap.sh "lobsters" "https://lobste.rs/rss"
$CMDD/runscrap.sh "journalduhacker" "https://www.journalduhacker.net/rss"
$CMDD/runscrap.sh "hackernews" "https://news.ycombinator.com/rss"
$CMDD/runscrap.sh "hackaday" "https://hackaday.com/blog/feed/"
$CMDD/runscrap.sh "francetvinfo" "https://www.francetvinfo.fr/titres.rss"
$CMDD/runscrap.sh "sebsauvage" "https://sebsauvage.net/links/?do=rss"
