#!/bin/sh -eu

# shellcheck disable=SC2086

CMDD=$(dirname "$(realpath $0)")

FEED_ID="$1"
FEED_URL="$2"

. $CMDD/env/env
[ -s $CMDD/env/${FEED_ID}.env ] && . $CMDD/env/${FEED_ID}.env

# 0: init db if needed
sqlite3 $DATA_DIR/${FEED_ID}.db < $CMDD/schema.sql

# 1: scrap data
scrapy crawl -L $LOG_LEVEL -t db -o /dev/null feed -a feedurl="$FEED_URL" -a feedid="$FEED_ID"

# 2:  build feed
PYTHONPATH="$(realpath $CMDD/..)" \
$CMDD/build_atom.py $FEED_ID "${DATA_DIR}/${FEED_ID}.db" $FEED_NB_ENTRIES | gzip > "${DATA_DIR}/${FEED_ID}.atom.gz"

# 3: upload
$CMDD/ftpupload.sh "${DATA_DIR}/${FEED_ID}.atom.gz" "${FTP_DIR}"
