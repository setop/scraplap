#!/bin/sh -eu

src_filepath="${1}"
dst_dir="${2}"

src_file=$(basename "${src_filepath}")
src_dir=$(dirname "${src_filepath}")
cd $src_dir

ftp -i -n -p $FTP_SERVER << EEE
quote USER $FTP_USER
quote PASS $FTP_PASS
binary
cd $dst_dir
mput $src_file
quit
EEE
