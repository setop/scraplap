BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `entry` (
	`id`	TEXT,
	`title`	TEXT,
	`pubdate`	INTEGER,
	`link`	TEXT,
	`contenttype`	INTEGER,
	`content`	TEXT,
	`tags` TEXT,
	PRIMARY KEY(`id`)
);
CREATE INDEX IF NOT EXISTS `query_order` ON `entry` (
	`pubdate`	DESC,
	`id`
);
--alter table entry add column `tags` TEXT;
COMMIT;
