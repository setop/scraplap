#!/usr/bin/env python3

import sys
import sqlite3
from xml.sax.saxutils import XMLGenerator
from datetime import datetime

UPPER_CONTENT_LIMIT = 15_000
CUTTING_LEVEL = 8_000


def build_leaf_tag(xg: XMLGenerator, tag_name, attr={}, text=None):
	xg.startElement(tag_name, attr)
	if text is not None:
		xg.characters(text)
	xg.endElement(tag_name)


def format_begin(xg: XMLGenerator, t):
	build_leaf_tag(xg, "id", {}, "tag:zoocoop.com,2018:%s" % t)
	build_leaf_tag(xg, "title", {}, t.capitalize())
	build_leaf_tag(xg, "updated", {}, datetime.now().isoformat(timespec='seconds')+"Z")


def format_entry(xg: XMLGenerator, feed_name, i, t, d, l, tg, ct, c, n):
	xg.startElement("entry", {})
	build_leaf_tag(xg, "id", {}, "http://www.zoocoop.com/contentoob/%s/%s" % (feed_name, i))
	build_leaf_tag(xg, "title", {}, t)
	build_leaf_tag(xg, "link",  {"href": l})
	build_leaf_tag(xg, "updated", {}, datetime.utcfromtimestamp(d).isoformat(timespec='seconds')+"Z")
	# domain of the linked article as author (see issue #12)
	xg.startElement("author", {})
	build_leaf_tag(xg, "name", {}, l.split("/")[2])  # [ 'http:', '', fqdn, uri ]
	xg.endElement("author")
	xg.startElement("content", {"type": "html"} if ct.split("/")[1] == "html" else {})
	# if content length > UPPER_CONTENT_LIMIT, cut to CUTTING_LEVEL and inform reader
	# n is none for empty content (translated from null)
	if n is not None and n > UPPER_CONTENT_LIMIT:
		xg.characters("<div>NOTE: this article is %d chars long, only the %d firsts are inlined, read the whole of it online</div><hr>" % (n, CUTTING_LEVEL))
		xg.characters(c[:CUTTING_LEVEL])
		xg.characters("[...]")
	else:
		xg.characters(c)
	xg.endElement("content")
	if tg is not None:
		for tag in tg.split(","):
			build_leaf_tag(xg, "category", {}, tag)
	xg.endElement("entry")


def set_env():
	import os
	global UPPER_CONTENT_LIMIT, CUTTING_LEVEL
	if "UPPER_CONTENT_LIMIT" in os.environ:
		UPPER_CONTENT_LIMIT = int(os.environ["UPPER_CONTENT_LIMIT"])
	if "CUTTING_LEVEL" in os.environ:
		CUTTING_LEVEL = int(os.environ["CUTTING_LEVEL"])


def main(feed_name, db_path, n=25):
	set_env()
	conn = sqlite3.connect(db_path)
	cur = conn.execute("select id, title, pubdate, link, tags, contenttype, substr(content, 0, %d), length(content) from entry order by pubdate DESC, id LIMIT %d" % (UPPER_CONTENT_LIMIT, n))
	xg = XMLGenerator(sys.stdout, "utf-8")
	xg.startDocument()
	xg.startElement("feed", {"xmlns": "http://www.w3.org/2005/Atom"})
	format_begin(xg, feed_name)
	for r in cur:
		format_entry(xg, feed_name, *r)
	xg.endElement("feed")
	xg.endDocument()
	conn.close()


if __name__ == '__main__':
	if len(sys.argv) > 3:
		main(sys.argv[1], sys.argv[2], int(sys.argv[3]))
	else:
		main(sys.argv[1], sys.argv[2])
