#!/bin/bash

# set env as required by pam_env.so
printenv >> /etc/environment

# place crontab
# /!\ /!\ /!\ /!\
# server is probably UTC timezone
# % must be escaped for printf with %%
# **and** % must be escaped for cron with \%
# but \ must be escaped for printf with \\
# => \\%%
# especially true for date pattern
# /!\ /!\ /!\ /!\
(
printf 'SHELL="/bin/bash"\n'
printf 'PATH="%s"\n' $PATH ;
printf 'ping="curl -fsS --retry 3 https://hc-ping.com/13a73a04-5d59-42f2-95ec-b24205fc0ea4"\n'
printf '15 5-23 * * * cd /scraplap; ./scripts/runjob.sh >> /tmp/scraplap.log.$(date +\\%%j) 2>&1 && ${ping} || ${ping}/fail \n';
) | crontab

# 17 -> 19:48 CEST

# start cron
exec /usr/sbin/cron -f
